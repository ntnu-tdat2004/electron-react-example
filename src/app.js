// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import {Component} from 'react-simplified';
import {HashRouter, Route} from 'react-router-dom';
import createHashHistory from 'history/createHashHistory';
const history = createHashHistory();
import {Customer, customerService, Item, inventoryService} from './services';
import {NavigationBar, Table, Card} from './widgets';

class Home extends Component {
  render() {
    return <Card title="Example App">Demonstration of Electron, React and Flow</Card>;
  }
}

class Customers extends Component<{}> {
  table;

  render() {
    return (
      <div>
        <Table
          ref={e => (this.table = e)}
          header={['First Name', 'Last Name', 'City']}
          onRowClick={this.selectCustomer}
        />
      </div>
    );
  }

  mounted() {
    customerService.getCustomers().then(customers => {
      // $FlowFixMe
      this.table?.setRows(
        customers.map(customer => ({id: customer.id, cells: [customer.firstName, customer.lastName, customer.city]}))
      );
    });
  }

  selectCustomer(id) {
    history.push('/customers/' + id);
  }
}

class Inventory extends Component {
  table;

  render() {
    return (
      <div>
        <Table ref={e => (this.table = e)} header={['Item', 'Quantity']} onRowClick={this.selectItem} />
      </div>
    );
  }

  mounted() {
    inventoryService.getItems().then(items => {
      // $FlowFixMe
      this.table?.setRows(items.map(item => ({id: item.id, cells: [item.name, item.quantity]})));
    });
  }

  selectItem(id) {
    history.push('/inventory/' + id);
  }
}

class CustomerDetails extends Component<{match: {params: {id: number}}}> {
  customer: ?Customer = null;

  render() {
    if (!this.customer) return null;
    return (
      <Card title="Customer">
        {this.customer.firstName + ' ' + this.customer.lastName + ', city: ' + this.customer.city}
      </Card>
    );
  }

  mounted() {
    customerService.getCustomer(this.props.match.params.id).then(customer => {
      this.customer = customer;
    });
  }
}

class InventoryItem extends Component<{match: {params: {id: number}}}> {
  item: ?Item = null;

  render() {
    if (!this.item) return null;
    return <Card title="Inventory Item">{this.item.name + ', quantity: ' + this.item.quantity} </Card>;
  }

  mounted() {
    inventoryService.getItem(this.props.match.params.id).then(item => {
      this.item = item;
    });
  }
}

let root = document.getElementById('root');
if (root) {
  ReactDOM.render(
    <HashRouter>
      <div>
        <NavigationBar
          brand="Electron React Example"
          links={[{to: '/customers', text: 'Customers'}, {to: '/inventory', text: 'Inventory'}]}
        />
        <Route exact path="/" component={Home} />
        <Route path="/customers" component={Customers} />
        <Route exact path="/customers/:id" component={CustomerDetails} />
        <Route path="/inventory" component={Inventory} />
        <Route exact path="/inventory/:id" component={InventoryItem} />
      </div>
    </HashRouter>,
    root
  );
}
