// @flow

export class Customer {
  static nextId = 1;
  id: number;
  firstName: string;
  lastName: string;
  city: string;

  constructor(firstName: string, lastName: string, city: string) {
    this.id = Customer.nextId++;
    this.firstName = firstName;
    this.lastName = lastName;
    this.city = city;
  }
}

let customers = [new Customer('Ola', 'Nordmann', 'Oslo'), new Customer('Kari', 'Jensen', 'Trondheim'), new Customer('Per', 'Person', 'Bergen')];
/**
 * Data is stored in memory
 */
class CustomerService {
  getCustomers(): Promise<Customer[]> {
    return new Promise(resolve => {
      resolve(customers);
    });
  }

  getCustomer(id: number): Promise<Customer> {
    return new Promise(resolve => {
      for (let customer of customers) {
        if (id == customer.id) {
          resolve(customer);
          break;
        }
      }
    });
  }
}
export let customerService = new CustomerService();

export class Item {
  static nextId = 1;
  id: number;
  name: string;
  quantity: number;

  constructor(name: string, quantity: number) {
    this.id = Item.nextId++;
    this.name = name;
    this.quantity = quantity;
  }
}

let items = [new Item('Eple', 150), new Item('Appelsin', 100)];
/**
 * Data is stored in memory
 */
class InventoryService {
  getItems(): Promise<Item[]> {
    return new Promise(resolve => {
      resolve(items);
    });
  }

  getItem(id: number): Promise<Item> {
    return new Promise(resolve => {
      for (let item of items) {
        if (id == item.id) {
          resolve(item);
          break;
        }
      }
    });
  }
}
export let inventoryService = new InventoryService();
