// @flow
import * as React from 'react';
import ReactDOM from 'react-dom';
import {Component} from 'react-simplified';
import {NavLink} from 'react-router-dom';

/**
 * A widget (reusable component) that renders a navigation bar using Bootstrap classes
 */
export class NavigationBar extends Component<{
  brand?: React.Node,
  links: {to: string, text: React.Node, exact?: boolean}[]
}> {
  render() {
    return (
      <nav className="navbar navbar-expand-sm bg-light navbar-light">
        {this.props.brand ? (
          <NavLink className="navbar-brand" activeClassName="active" to="/">
            {this.props.brand}
          </NavLink>
        ) : null}
        <ul className="navbar-nav">
          {this.props.links.map((link, i) => (
            <li key={i}>
              <NavLink className="nav-link" activeClassName="active" exact={link.exact} to={link.to}>
                {link.text}
              </NavLink>
            </li>
          ))}
        </ul>
      </nav>
    );
  }
}

/**
 * A widget (reusable component) that renders a table using Bootstrap classes
 */
type TableRow = {id: number, cells: React.Node[]}; // Helper type
export class Table extends Component<{header?: React.Node[], onRowClick?: number => void}> {
  rows: TableRow[] = [];

  setRows(rows: TableRow[]) {
    this.rows = rows;
  }

  render() {
    return (
      <table className="table table-hover">
        {this.props.header ? (
          <thead>
            <tr>{this.props.header.map((title, i) => <th key={i}>{title}</th>)}</tr>
          </thead>
        ) : null}
        <tbody>
          {this.rows.map(row => (
            // $FlowFixMe
            <tr key={row.id} onClick={() => this.props?.onRowClick(row.id)}>
              {row.cells.map((cell, i) => <td key={i}>{cell}</td>)}
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}

/**
 * A widget (reusable component) that renders an information card using Bootstrap classes
 */
export class Card extends Component<{title: React.Node, children?: React.Node}> {
  render() {
    return (
      <div className="card">
        <div className="card-body">
          <h5 className="card-title">{this.props.title}</h5>
          <div className="card-text">{this.props.children}</div>
        </div>
      </div>
    );
  }
}
