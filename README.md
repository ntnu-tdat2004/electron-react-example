# Electron React Example

## Instructions
1. Install [Atom](https://atom.io/) and [Node.js](https://nodejs.org/en/).
2. Install the Flow static type checker, and the Atom addon Nuclide:
    ```sh
    npm install -g flow-bin
    apm install nuclide
    ```
3. Download and run example, and open source folder in Atom:
    ```sh
    git clone https://gitlab.com/ntnu-tdat2004/electron-react-example
    cd electron-react-example
    npm install  # Install dependencies
    npm start&   # Start example-application. Refresh application with control/command-r
    atom .       # Open example-folder in atom
    ```
    Note that only the JavaScript files in the src-folder are checked for type-errors through Flow.
